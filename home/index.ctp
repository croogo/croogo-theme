<div id="homepage-boxes">
    <div class="container_12">
        <div class="grid_4">
            <div class="box border-right">
                <h3>GitHub</h3>

                <p>
                    Fork the project from <a href="http://github.com/croogo/croogo">GitHub</a> if you are interested in contributing,
                    or you can simply watch its progress.
                </p>

                <div class="box-image github">
                    <a href="http://github.com/croogo/croogo"><img src="https://github.com/github/media/raw/master/logos/github_logo_social_coding_outlined.png" style="width: 115px;" alt="GitHub" /></a>
                </div>
            </div>
        </div>

        <div class="grid_4">
            <div class="box border-right">
                <h3>Lighthouse</h3>

                <p>
                    All development related tickets are maintained at <a href="http://croogo.lighthouseapp.com/">Lighthouse</a> website.
                    You are supposed to report bugs here.
                </p>

                <div class="box-image lighthouse">
                    <a href="http://croogo.lighthouseapp.com/"><img src="http://lighthouseapp.com/marketing/images/logo.png" alt="Lighthouse" /></a>
                </div>
            </div>

        </div>

        <div class="grid_4">
            <div class="box">
                <h3>Google Group</h3>

                <p>
                    We also have a small but growing community which is pretty active.
                    You can join our mailing list on <a href="http://groups.google.com/group/croogo">Google Group</a>.
                </p>

                <div class="box-image google-group">
                    <a href="http://groups.google.com/group/croogo"><img src="http://groups.google.com/intl/en/images/logos/groups_logo.gif" alt="Google Group" /></a>
                </div>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</div>